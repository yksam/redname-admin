package com.zj003.module.sys.service.impl;

import com.zj003.module.sys.service.SysPostService;
import org.springframework.stereotype.Service;

/**
 * @author RedName
 * @description 针对表【sys_post(岗位信息表)】的数据库操作Service实现
 * @createDate 2022-05-01 03:30:07
 */
@Service
public class SysPostServiceImpl implements SysPostService {

}
