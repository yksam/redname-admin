/*
 * 文 件 名:  ServletUtils
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.util.web;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;

/**
 * <功能详细描述>
 * 客户端工具类
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ServletUtils {
    /**
     * 获取请求属性
     *
     * @return org.springframework.web.context.request.ServletRequestAttributes
     * @author RedName
     * {@code @date} 2022/5/6 2:25
     */
    public static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    /**
     * 获取request
     *
     * @return javax.servlet.http.HttpServletRequest
     * @author RedName
     * {@code @date} 2022/5/6 2:25
     */
    public static HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }


    /**
     * 获取response
     *
     * @return javax.servlet.http.HttpServletResponse
     * @author RedName
     * {@code @date} 2022/5/6 2:34
     */
    public static HttpServletResponse getResponse() {
        return getRequestAttributes().getResponse();
    }

    /**
     * 获取session
     *
     * @return javax.servlet.http.HttpSession
     * @author RedName
     * {@code @date} 2022/5/6 2:34
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

}
