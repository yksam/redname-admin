/*
 * 文 件 名:  RestPathController
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/7
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.annotation.web;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/7]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@RequestMapping
public @interface RestPathController {
    @AliasFor("path") String[] value() default {};

    @AliasFor("value") String[] path() default {};
}
