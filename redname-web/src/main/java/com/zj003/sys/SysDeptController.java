/*
 * 文 件 名:  SysDeptController
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  RedName
 * 修改时间:  2022/5/15
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.sys;

import com.zj003.common.annotation.web.RestPathController;
import com.zj003.common.core.base.BaseController;
import com.zj003.common.domain.entity.SysDept;
import com.zj003.common.domain.result.Result;
import com.zj003.module.sys.service.SysDeptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/15]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */

@Slf4j
@RestPathController("/sys/dept")
@RequiredArgsConstructor
public class SysDeptController extends BaseController {

    private final SysDeptService deptService;


    @GetMapping("list")
    public Result list(SysDept dept) {
        if (StringUtils.isNotBlank(dept.getDeptName()) || StringUtils.isNotBlank(dept.getDeptName())) {
            return Result.success(deptService.selectDeptList(dept));
        }
        return Result.success(deptService.selectDeptTree());
    }

    @GetMapping("tree")
    public Result tree() {
        List<SysDept> deptTree = deptService.selectDeptTree();
        return Result.success(deptTree);
    }

    @GetMapping("{deptId}")
    public Result findByDeptId(@PathVariable Long deptId) {
        SysDept sysDept = deptService.selectDeptByDeptId(deptId);
        return Result.success(sysDept);
    }

    @PostMapping
    public Result save(@RequestBody SysDept dept) {
        deptService.insertDept(dept);
        return Result.success();
    }

    @PutMapping
    public Result update(@RequestBody SysDept dept) {
        deptService.updateDept(dept);
        return Result.success();
    }

    @DeleteMapping("{deptId}")
    public Result deleteByDeptId(@PathVariable Long deptId) {
        deptService.deleteDept(deptId);
        return Result.success();
    }
}
