/*
 * 文 件 名:  RedNameApplication
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/1]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SpringBootApplication
public class RedNameApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedNameApplication.class, args);
    }
}
