/*
 * 文 件 名:  Jwt
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/3
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.domain.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.Min;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/3]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "redname.jwt")
public class Jwt {

    /**
     * HTTP 报头的认证字段的 key
     */
    private String header = "Authorization";

    /**
     * HTTP 报头的认证字段的值的前缀
     */
    private String prefix = "Bearer ";

    /**
     * Access Token 过期时间
     */
    @Min(1)
    private long expireTime = 30;

    /**
     * 签名
     */
    private String key;

}
