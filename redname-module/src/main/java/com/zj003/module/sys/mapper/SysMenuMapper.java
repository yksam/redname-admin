package com.zj003.module.sys.mapper;

import com.zj003.common.domain.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author RedName
 * @description 针对表【sys_menu(菜单权限表)】的数据库操作Mapper
 * @createDate 2022-05-01 03:30:07
 * @Entity com.zj003.module.sys.domain.SysMenu
 */
@Mapper
@Repository
public interface SysMenuMapper {


    /**
     * 根据用户ID查询权限
     *
     * @param userId: 用户ID
     * @return java.util.Set<java.lang.String>
     * @author RedName
     * {@code @date} 2022/5/5 5:09
     */
    Set<String> selectMenuPermsByUserId(@Param("userId") Long userId);

    /**
     * 根据用户ID查询菜单
     *
     * @param userId:
     * @return java.util.List<com.zj003.common.domain.entity.SysMenu>
     * @author RedName
     * {@code @date} 2022/5/6 6:43
     */
    List<SysMenu> selectMenuTreeByUserId(@Param("userId") Long userId);

    /**
     * 查询所有菜单
     *
     * @return java.util.List<com.zj003.common.domain.entity.SysMenu>
     * @author RedName
     * {@code @date} 2022/5/7 3:34
     */
    List<SysMenu> selectMenuTreeAll();

    /**
     * 查询系统菜单列表
     *
     * @param menu:
     * @return java.util.List<com.zj003.common.domain.entity.SysMenu>
     * @author RedName
     * {@code @date} 2022/5/6 6:45
     */
    List<SysMenu> selectMenuList(SysMenu menu);


}
