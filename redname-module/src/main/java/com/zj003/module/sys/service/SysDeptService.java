package com.zj003.module.sys.service;

import com.zj003.common.domain.entity.SysDept;

import java.util.List;

/**
 * @author RedName
 * @description 针对表【sys_dept(部门表)】的数据库操作Service
 * @createDate 2022-05-01 03:30:07
 */
public interface SysDeptService {
    /**
     * 查询所有部门
     *
     * @param dept:
     * @return java.util.List<com.zj003.common.domain.entity.SysDept>
     * @author RedName
     * {@code @date} 2022/5/14 23:35
     */
    List<SysDept> selectDeptList(SysDept dept);

    /**
     * 查询部门树
     *
     * @return java.util.List<com.zj003.common.domain.entity.SysDept>
     * @author RedName
     * {@code @date} 2022/5/14 23:35
     */
    List<SysDept> selectDeptTree();

   /**
    * 根据部门id查询
    * @author RedName
    * {@code @date} 2022/5/15 2:52
    * @param deptId:
    * @return com.zj003.common.domain.entity.SysDept
    */
    SysDept selectDeptByDeptId(Long deptId);

    /**
     * 添加部门
     *
     * @param dept:
       * @author RedName
     * {@code @date} 2022/5/14 23:39
     */
    void insertDept(SysDept dept);

    /**
     * 更新部门
     *
     * @param dept:
     * @author RedName
     * {@code @date} 2022/5/14 23:39
     */
    void updateDept(SysDept dept);

    /**
     * 根据部门ID删除部门
     *
     * @param deptId:
     * @return void
     * @author RedName
     * {@code @date} 2022/5/14 23:39
     */
    void deleteDept(Long deptId);


}
