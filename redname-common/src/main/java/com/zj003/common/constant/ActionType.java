/*
 * 文 件 名:  ActionType
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  RedName
 * 修改时间:  2022/5/16
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.constant;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/16]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ActionType {


    public static final String QUERY = "QUERY";

    public static final String INSERT = "INSERT";

    public static final String UPDATE = "UPDATE";

    public static final String DELETE = "DELETE";
}
