/*
 * 文 件 名:  MimeType
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.constant;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MimeType {

    public static final String APPLICATION_JSON_CHARSET = "application/json;charset=UTF-8";

    public static final String APPLICATION_JSON = "application/json";

}
