/*
 * 文 件 名:  BaseController
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.core.base;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zj003.common.constant.PageConstants;
import com.zj003.common.domain.result.RowList;
import com.zj003.common.util.web.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * <功能详细描述>
 * 基础控制器
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class BaseController {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * date数据转换
     *
     * @param binder:
     * @author RedName
     * {@code @date} 2022/5/6 23:39
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtil.parseDate(text));
            }
        });
    }

    /**
     * 设置分页
     *
     * @author RedName
     * {@code @date} 2022/5/7 1:04
     */
    protected void startPage() {
        startPage(ServletUtils.getRequest());
    }

    /**
     * 设置分页
     *
     * @param request:
     * @author RedName
     * {@code @date} 2022/5/7 1:04
     */
    protected void startPage(HttpServletRequest request) {
        try {
            JsonNode jsonNode = objectMapper.readTree(request.getInputStream());

            int pageNum = jsonNode.get(PageConstants.PAGE_NUM).intValue();
            int pageSize = jsonNode.get(PageConstants.PAGE_SIZE).intValue();
            startPage(pageNum, pageSize);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 设置分页
     *
     * @param pageNum:  当前页
     * @param pageSize: 每页数
     * @author RedName
     * {@code @date} 2022/5/7 1:05
     */
    protected void startPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
    }


    /**
     * 构建返回List对象
     *
     * @param list:
     * @return com.zj003.common.domain.result.RowList
     * @author RedName
     * {@code @date} 2022/5/7 1:06
     */
    protected RowList getRowList(List<?> list) {
        RowList rowList = new RowList();
        rowList.setRows(list);
        rowList.setTotal(new PageInfo<>(list).getTotal());
        return rowList;
    }
}
