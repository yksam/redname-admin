/*
 * 文 件 名:  NetworkHttp
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.http;

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Var;
import lombok.Getter;
import lombok.Setter;

/**
 * <功能详细描述>
 * 互联网工具接口请求
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface NetworkHttp {


    /**
     * 根据ip查询ip详细信息
     *
     * @param ip:
     * @return java.lang.String
     * @author RedName
     * {@code @date} 2022/5/4 4:37
     */
    @Get(url = "https://whois.pconline.com.cn/ipJson.jsp?json=true&ip={ip}", dataType = "json")
    IpInfo queryIp(@Var("ip") String ip);


    @Setter
    @Getter
    class IpInfo {

        /**
         * ip
         */
        private String ip;
        /**
         * 省份
         */
        private String pro;
        /**
         * 省份编码
         */
        private String proCode;
        /**
         * 市
         */
        private String city;
        /**
         * 市编码
         */
        private String cityCode;
        /**
         * 地区
         */
        private String region;
        /**
         * 地区编码
         */
        private String regionCode;
        /**
         * ip地址
         */
        private String addr;
        /**
         * 地区名称
         */
        private String regionNames;
        /**
         * 错误信息
         */
        private String err;
    }

}
