/*
 * 文 件 名:  TokenService
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/3
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.framework.service;


import com.zj003.common.domain.model.LoginUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/3]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */

public interface TokenService {

    /**
     * 设置用户身份信息
     *
     * @param loginUser:
     * @author RedName
     * {@code @date} 2022/5/6 2:57
     */
    void setLoginUser(LoginUser loginUser);

    /**
     * 获取用户身份信息
     *
     * @param request:
     * @return java.util.Optional<com.zj003.common.domain.model.LoginUser>
     * @author RedName
     * {@code @date} 2022/5/6 3:25
     */
    LoginUser getLoginUser(HttpServletRequest request);
    /**
     * 获取用户身份信息
     *
     * @param request:
     * @return java.util.Optional<com.zj003.common.domain.model.LoginUser>
     * @author RedName
     * {@code @date} 2022/5/6 3:25
     */
    Optional<LoginUser> getLoginUserOptional(HttpServletRequest request);

    /**
     * 删除用户身份信息
     *
     * @param token:
     * @author RedName
     * {@code @date} 2022/5/6 2:58
     */
    void delLoginUser(String token);

    /**
     * 创建令牌
     *
     * @param loginUser:
     * @return java.lang.String
     * @author RedName
     * {@code @date} 2022/5/6 2:02
     */
    String createToken(LoginUser loginUser);

    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     *
     * @param loginUser:
     * @author RedName
     * {@code @date} 2022/5/6 2:55
     */
    void verifyToken(LoginUser loginUser);
}
