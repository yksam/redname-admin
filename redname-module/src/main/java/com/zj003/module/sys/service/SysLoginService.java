/*
 * 文 件 名:  SysLoginService
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.module.sys.service;

/**
 * <功能详细描述>
 * 登录校验方法
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface SysLoginService {


    /**
     * 登陆验证
     *
     * @param username 用户名
     * @param password 密码
     * @return java.lang.String
     * @author RedName
     * {@code @date} 2022/5/6 3:30
     */
    String login(String username, String password);

}
