/*
 * 文 件 名:  CorsConfig
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/4/30
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.framework.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

/**
 * <功能详细描述>
 * 解决跨域请求
 *
 * @author ReaName
 * @version [版本号, 2022/4/30]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration

public class CorsConfig {

    @Bean
    @Primary
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Collections.singletonList("*"));
        configuration.addAllowedOrigin("*");
        configuration.addExposedHeader("Authorization");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    //@Bean
    //public CorsWebFilter corsWebFilter() {
    //
    //    //创建基于URL的跨域配置
    //    UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
    //
    //    //配置详情
    //    CorsConfiguration corsConfiguration = new CorsConfiguration();
    //    corsConfiguration.setAllowCredentials(true);
    //
    //    //设置允许跨域的请求消息头
    //    corsConfiguration.addAllowedHeader("*");
    //    //设置允许跨域请求的方法
    //    corsConfiguration.addAllowedMethod("*");
    //    //设置允许跨域请求的来源
    //    corsConfiguration.addAllowedOrigin("*");
    //    //是否允许携带cookie进行跨域
    //    corsConfiguration.setAllowCredentials(true);
    //    corsConfiguration.addExposedHeader("Authorization");
    //
    //    //配置跨域的路径和跨域配置
    //    configurationSource.registerCorsConfiguration("/**", corsConfiguration);
    //
    //
    //    return new CorsWebFilter(configurationSource);
    //}
}
