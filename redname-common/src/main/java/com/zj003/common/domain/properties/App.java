/*
 * 文 件 名:  AppConfig
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.domain.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "redname.app")
public class App {

    /**
     * 是否联网查询地址
     */
    private boolean addressEnabled = false;

    /**
     *
     */
    private String appName = "redName";


}
