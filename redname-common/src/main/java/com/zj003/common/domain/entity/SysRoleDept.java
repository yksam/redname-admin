package com.zj003.common.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色和部门关联表
 * @author RedName
 * @TableName sys_role_dept
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRoleDept implements Serializable {

    private static final long serialVersionUID = -474847041609655719L;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 部门ID
     */
    private Long deptId;


}