package com.zj003.common.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户与岗位关联表
 * @author RedName
 * @TableName sys_user_post
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserPost implements Serializable {

    private static final long serialVersionUID = -1592123733727074630L;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 岗位ID
     */
    private Long postId;


}