/*
 * 文 件 名:  AddressUtils
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.util.ip;

import com.dtflys.forest.Forest;
import com.zj003.common.http.NetworkHttp;
import lombok.extern.slf4j.Slf4j;

/**
 * <功能详细描述>
 * 获取地址类
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class AddressUtils {

    /**
     * 未知地址
     */
    private static final String UNKNOWN = "XX XX";

    /**
     * ip请求
     */
    private static final NetworkHttp NETWORK_HTTP = Forest.client(NetworkHttp.class);


    public static String getRealAddressByIp(String ip, boolean isNetwork) {
        if (IpUtils.internalIp(ip)) {
            return "内网IP";
        }
        if (isNetwork) {
            NetworkHttp.IpInfo ipInfo = NETWORK_HTTP.queryIp(ip);
            return String.format("%s %s %s", ipInfo.getPro(), ipInfo.getCity(), ipInfo.getRegion());

        }
        return UNKNOWN;
    }

}
