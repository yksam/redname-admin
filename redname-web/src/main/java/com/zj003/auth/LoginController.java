/*
 * 文 件 名:  LoginController
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.auth;

import com.zj003.common.domain.entity.SysMenu;
import com.zj003.common.domain.entity.SysUser;
import com.zj003.common.domain.model.LoginBody;
import com.zj003.common.domain.model.LoginUser;
import com.zj003.common.domain.result.Result;
import com.zj003.common.util.SecurityUtils;
import com.zj003.common.util.web.ServletUtils;
import com.zj003.framework.service.TokenService;
import com.zj003.module.sys.service.SysLoginService;
import com.zj003.module.sys.service.SysMenuService;
import com.zj003.module.sys.service.SysPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * <功能详细描述>
 * 登陆
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class LoginController {

    private final SysLoginService loginService;
    private final TokenService tokenService;

    private final SysPermissionService permissionService;

    /**
     * 登陆
     *
     * @param loginBody:
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/6 7:34
     */
    @PostMapping("/login")
    public Result login(@RequestBody @Valid LoginBody loginBody) {
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword());
        Result success = Result.success();
        success.put("token", token);
        return success;
    }


    /**
     * 获取用户信息
     *
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/6 7:34
     */
    @GetMapping("getInfo")
    public Result getInfo() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        // 角色集合
        //Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user.getUserId());
        Result result = Result.success();
        result.put("user", user);
        //result.put("roles", roles);
        result.put("permissions", permissions);
        return result;
    }
}
