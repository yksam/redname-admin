/*
 * 文 件 名:  SysPermissionServiceImpl
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/5
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.module.sys.service.impl;

import com.zj003.module.sys.mapper.SysMenuMapper;
import com.zj003.module.sys.service.SysPermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/5]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysPermissionServiceImpl implements SysPermissionService {

    private final SysMenuMapper menuMapper;

    @Override
    public Set<String> getMenuPermission(Long userId) {
        return menuMapper.selectMenuPermsByUserId(userId);
    }
}
