package com.zj003.module.sys.mapper;

import java.util.List;
import java.util.Optional;

import org.apache.ibatis.annotations.Param;
import com.zj003.common.domain.entity.SysUser;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author RedName
 * @description 针对表【sys_user(用户信息表)】的数据库操作Mapper
 * @createDate 2022-05-01 03:30:07
 * @Entity com.zj003.module.sys.domain.SysUser
 */
@Mapper
@Repository
public interface SysUserMapper {


    /**
     * 根据用户名查询用户信息
     *
     * @param userName:
     * @return java.util.Optional<com.zj003.common.domain.entity.SysUser>
     * @author RedName
     * {@code @date} 2022/5/4 23:39
     */
    Optional<SysUser> selectByUserName(@Param("userName") String userName);


    /**
     * 根据用户名更新密码
     *
     * @param password:
     * @param userName:
     * @return int
     * @author RedName
     * {@code @date} 2022/5/5 1:47
     */
    int updatePasswordByUserName(@Param("password") String password, @Param("userName") String userName);
}
