/*
 * 文 件 名:  SysMenuController
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/7
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.sys;

import com.zj003.common.annotation.web.RestPathController;
import com.zj003.common.core.base.BaseController;
import com.zj003.common.domain.entity.SysMenu;
import com.zj003.common.domain.entity.SysUser;
import com.zj003.common.domain.model.LoginUser;
import com.zj003.common.domain.result.Result;
import com.zj003.common.util.SecurityUtils;
import com.zj003.common.util.web.ServletUtils;
import com.zj003.framework.service.TokenService;
import com.zj003.module.sys.service.SysMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * <功能详细描述>
 * 系统菜单控制器
 *
 * @author ReaName
 * @version [版本号, 2022/5/7]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RestPathController("/sys/menu")
@RequiredArgsConstructor
public class SysMenuController extends BaseController {

    private final TokenService tokenService;
    private final SysMenuService menuService;

    /**
     * 获取路由信息
     *
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/7 1:17
     */
    @GetMapping("/nav")
    public Result nav() {
        LoginUser loginUser = SecurityUtils.getLoginUser();

        // 用户信息
        SysUser user = loginUser.getUser();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(user.getUserId());
        return Result.success(menus);
    }


}
