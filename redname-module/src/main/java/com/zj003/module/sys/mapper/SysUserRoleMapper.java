package com.zj003.module.sys.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author RedName
 * @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Mapper
 * @createDate 2022-05-01 03:30:07
 * @Entity com.zj003.module.sys.domain.SysUserRole
 */
@Mapper
@Repository
public interface SysUserRoleMapper {


}
