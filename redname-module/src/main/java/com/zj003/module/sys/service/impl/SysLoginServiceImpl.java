/*
 * 文 件 名:  SysLoginServiceImpl
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.module.sys.service.impl;

import com.zj003.common.domain.model.LoginUser;
import com.zj003.framework.service.TokenService;
import com.zj003.module.sys.service.SysLoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysLoginServiceImpl implements SysLoginService {


    private final TokenService tokenService;

    private final AuthenticationManager authenticationManager;

    @Override
    public String login(String username, String password) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);

        // 该方法会去调用UserDetailsService.loadUserByUsername
        Authentication authenticate = authenticationManager.authenticate(authentication);

        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        return tokenService.createToken(loginUser);
    }
}
