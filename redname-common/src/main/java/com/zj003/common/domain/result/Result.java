/*
 * 文 件 名:  Result
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.domain.result;

import com.zj003.common.constant.HttpStatus;
import org.apache.commons.lang3.ObjectUtils;

import java.util.HashMap;

/**
 * <功能详细描述>
 * 操作消息提醒
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Result extends HashMap<String, Object> {

    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";

    /**
     * 返回内容
     */
    public static final String MSG_TAG = "message";

    /**
     * 数据对象
     */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 Result 对象，使其表示一个空消息。
     *
     * @author RedName
     * {@code @date} 2022/5/4 0:20
     */
    public Result() {
    }

    /**
     * 始化一个新创建的 Result 对象
     *
     * @param code:    状态码
     * @param message: 返回内容
     * @author RedName
     * {@code @date} 2022/5/4 0:21
     */
    public Result(int code, String message) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, message);
    }

    /**
     * 初始化一个新创建的 Result 对象
     *
     * @param code    状态码
     * @param message 返回内容
     * @param data    数据对象
     * @author RedName
     * {@code @date} 2022/5/4 0:22
     */
    public Result(int code, String message, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, message);
        if (ObjectUtils.isNotEmpty(data)) {
            super.put(DATA_TAG, data);
        }
    }


    /**
     * 返回成功消息
     *
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:26
     */
    public static Result success() {
        return Result.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @param data:
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/6 7:32
     */
    public static Result success(Object data) {
        return Result.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param message:
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:27
     */
    public static Result success(String message) {
        return Result.success(message, null);
    }

    /**
     * 返回成功消息
     *
     * @param message 返回内容
     * @param data    数据对象
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:29
     */
    public static Result success(String message, Object data) {
        return new Result(HttpStatus.SUCCESS, message, data);
    }

    /**
     * 返回错误消息
     *
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:30
     */
    public static Result error() {
        return Result.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param message: 返回内容
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:31
     */
    public static Result error(String message) {
        return Result.error(message, null);
    }

    /**
     * 返回错误消息
     *
     * @param message 返回内容
     * @param data    数据对象
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:32
     */
    public static Result error(String message, Object data) {
        return new Result(HttpStatus.ERROR, message, data);
    }


    /**
     * 返回错误消息
     *
     * @param code    状态码
     * @param message 返回内容
     * @return com.zj003.common.domain.result.Result
     * @author RedName
     * {@code @date} 2022/5/4 0:34
     */
    public static Result error(int code, String message) {
        return new Result(code, message, null);
    }


}
