/*
 * 文 件 名:  MenuType
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.constant;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MenuType {
    /**
     * 目录
     */
    public static final String CATALOGUE = "C";
    /**
     * 菜单
     */
    public static final String MENU = "M";
    /**
     * 按钮
     */
    public static final String BUTTON = "B";
}
