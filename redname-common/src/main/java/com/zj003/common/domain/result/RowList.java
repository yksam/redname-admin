/*
 * 文 件 名:  RowList
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/7
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.domain.result;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/7]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Getter
@Setter
public class RowList {


    /**
     * 总记录数
     */
    private long total;

    /**
     * 列表数据
     */
    private List<?> rows;
}
