/*
 * 文 件 名:  PropertiesConfig
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/3
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.framework.config;

import com.zj003.common.domain.properties.App;
import com.zj003.common.domain.properties.Jwt;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/3]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Setter
@Getter
@Validated
@Import({Jwt.class})
@Component
@ConfigurationProperties(prefix = "redname")
public class PropertiesConfig {


    /**
     * app配置属性
     */
    App app = new App();

    /**
     * jwt属性
     */
    Jwt jwt = new Jwt();

}
