/*
 * 文 件 名:  LoginBody
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.domain.model;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <功能详细描述>
 * 用户登录对象
 *
 * @author ReaName
 * @version [版本号, 2022/5/4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LoginBody {

    /**
     * 用户名
     */
    @NotNull
    @NotBlank
    private String username;

    /**
     * 用户密码
     */
    @NotNull
    @NotBlank
    private String password;


}
