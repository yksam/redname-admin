/*
 * 文 件 名:  SysPermissionService
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/5
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.module.sys.service;

import com.zj003.common.domain.entity.SysUser;

import java.util.Set;

/**
 * <功能详细描述>
 * 用户权限处理
 *
 * @author ReaName
 * @version [版本号, 2022/5/5]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface SysPermissionService {

    /**
     * 获取菜单数据权限
     *
     * @param userId:
     * @return java.util.Set<java.lang.String>
     * @author RedName
     * {@code @date} 2022/5/5 5:04
     */
    public Set<String> getMenuPermission(Long userId);
}
