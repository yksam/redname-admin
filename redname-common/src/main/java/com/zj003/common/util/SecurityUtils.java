/*
 * 文 件 名:  SecurityUtils
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/7
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.util;

import com.zj003.common.constant.HttpStatus;
import com.zj003.common.domain.model.LoginUser;
import com.zj003.common.exception.CustomException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/7]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SecurityUtils {

    /**
     * 获取用户名
     *
     * @return java.lang.String
     * @author RedName
     * {@code @date} 2022/5/10 0:41
     */
    public static String getUsername() {
        try {
            return getLoginUser().getUsername();
        } catch (Exception e) {
            throw new CustomException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取当前用户
     *
     * @return com.zj003.common.domain.model.LoginUser
     * @author RedName
     * {@code @date} 2022/5/10 0:40
     */
    public static LoginUser getLoginUser() {
        try {
            return (LoginUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new CustomException("获取用户信息异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取Authentication
     *
     * @return org.springframework.security.core.Authentication
     * @author RedName
     * {@code @date} 2022/5/7 3:20
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 是否为管理员
     *
     * @param userId:
     * @return boolean
     * @author RedName
     * {@code @date} 2022/5/7 3:20
     */
    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }
}
