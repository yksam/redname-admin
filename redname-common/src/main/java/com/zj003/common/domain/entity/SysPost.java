package com.zj003.common.domain.entity;

import com.zj003.common.core.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 岗位信息表
 *
 * @TableName sys_post
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysPost extends BaseEntity implements Serializable {


    private static final long serialVersionUID = -7968966194915617564L;
    /**
     * 岗位ID
     */
    private Long postId;

    /**
     * 岗位编码
     */
    private String postCode;

    /**
     * 岗位名称
     */
    private String postName;

    /**
     * 显示顺序
     */
    private Integer postSort;

    /**
     * 状态（0正常 1停用）
     */
    private String status;


    /**
     * 备注
     */
    private String remark;

}