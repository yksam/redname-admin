package com.zj003.module.sys.service;

import com.zj003.common.domain.entity.SysMenu;

import java.util.List;

/**
 * @author RedName
 * @description 针对表【sys_menu(菜单权限表)】的数据库操作Service
 * @createDate 2022-05-01 03:30:07
 */
public interface SysMenuService {


    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单列表 java.util.List<com.zj003.common.domain.entity.SysMenu>
     * @author RedName
     * {@code @date} 2022/5/6 6:28
     */
    List<SysMenu> selectMenuTreeByUserId(Long userId);
}
