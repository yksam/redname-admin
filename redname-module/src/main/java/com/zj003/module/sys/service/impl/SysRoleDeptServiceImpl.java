package com.zj003.module.sys.service.impl;

import com.zj003.module.sys.service.SysRoleDeptService;
import org.springframework.stereotype.Service;

/**
 * @author RedName
 * @description 针对表【sys_role_dept(角色和部门关联表)】的数据库操作Service实现
 * @createDate 2022-05-01 03:30:07
 */
@Service
public class SysRoleDeptServiceImpl implements SysRoleDeptService {

}
