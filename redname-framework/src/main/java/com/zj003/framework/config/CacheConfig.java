/*
 * 文 件 名:  CacheConfig
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  redName
 * 修改时间:  2022/5/1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.framework.config;

import org.springframework.context.annotation.Configuration;

/**
 * <功能详细描述>
 * 缓存配置
 *
 * @author ReaName
 * @version [版本号, 2022/5/1]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
public class CacheConfig {
    
}
