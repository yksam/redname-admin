/*
 * 文 件 名:  BaseEntity
 * 版    权:
 * 描    述:  <描述>
 * 修 改 人:  RedName
 * 修改时间:  2022/5/16
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.zj003.common.core.base;


import com.dtflys.forest.annotation.Get;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * <功能详细描述>
 *
 * @author ReaName
 * @version [版本号, 2022/5/16]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Getter
@Setter
public class BaseEntity {
    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;
}
