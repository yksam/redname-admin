package com.zj003.module.sys.mapper;

import org.apache.ibatis.annotations.Param;


import com.zj003.common.domain.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author RedName
 * @description 针对表【sys_dept(部门表)】的数据库操作Mapper
 * @createDate 2022-05-01 03:30:07
 * @Entity com.zj003.module.sys.domain.SysDept
 */
@Mapper
@Repository
public interface SysDeptMapper {

    /**
     * 查询所有部门
     *
     * @param dept:
     * @return java.util.List<com.zj003.common.domain.entity.SysDept>
     * @author RedName
     * {@code @date} 2022/5/14 23:30
     */
    List<SysDept> selectDeptList(SysDept dept);


    /**
     * 根据id查询部门
     *
     * @param deptId:
     * @return com.zj003.common.domain.entity.SysDept
     * @author RedName
     * {@code @date} 2022/5/15 3:30
     */
    SysDept selectDeptByDeptId(@Param("deptId") Long deptId);

    /**
     * 插入
     *
     * @param sysDept:
     * @return int
     * @author RedName
     * {@code @date} 2022/5/15 3:31
     */
    int insert(SysDept sysDept);

    /**
     * 根据部门ID选择更新
     *
     * @param sysDept:
     * @return int
     * @author RedName
     * {@code @date} 2022/5/15 3:34
     */
    int updateSelective(SysDept sysDept);


    /**
     * 根据主键删除
     *
     * @param deptId:
     * @return int
     * @author RedName
     * {@code @date} 2022/5/15 5:15
     */
    int deleteByDeptId(@Param("deptId") Long deptId);
}
